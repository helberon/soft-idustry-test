# How to get started with waarkoop-server test automation project

To run tests and work with this project, you will need a few things installed on your machine:

* Java: You'll need a recent JDK installed. JDK 1.8 or higher should be fine. (Don't forget to add required system variables)
* Maven: You will need Maven 3 or higher installed on your computer. (Don't forget to add required system variables)
* Git: All origin files live on GitLab and all changes upload to GitLab.
* IntelliJ IDE

Also, to work with project files you will need to install a few plugins (File>Settings>Plugins):
* Cucumber for Java
* Gherkin

# How to run automation tests locally

## 1. Maven terminal

Go to the Terminal tab and execute

```
mvn clean verify
```

## 2. TestRunner

Navigate to the TestRunner.class and run tests in standard or debug mode

![](../../Pictures/testRunnerImage.png)

## 3. Separate test scenarios

Go to the needed feature file, select and run scenario or feature you would like to run in standard or debug mode

# How to write your own scenarios

New tests can be written by adding new feature files or extending existing ones. This project uses Gherkin BDD format that allows to write new testing scenarios in `Given-When-Then` format. 
All feature files are stored in `src/test/resources/features` package. Add new scenario by creating a `.feature` file inside this package.

# Refactoring process
 
1. Gradle files were removed since project uses Maven as a building system
2. Feature file was separated into different scenarios to maintain test atomicity
3. Scenario steps related to api were moved to a separate class to avoid excess of code
4. Assertions became more specific for all endpoints
5. Base api URL was moved to serenity.conf file to make it general for all tests
6. Data model was added for response deserialization