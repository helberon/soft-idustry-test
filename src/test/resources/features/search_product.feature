Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario Outline: Search for the product without results
    When user calls endpoint <Product Name>
    Then he sees empty response
    Examples:
      | Product Name |
      | orange       |
      | apple        |

  Scenario Outline: Search for the product with results
    When user calls endpoint <Product Name>
    Then in the results he sees <Product Example>
    Examples:
      | Product Name | Product Example    |
      | cola         | Cola zero          |
      | pasta        | Pasta salade pesto |

  Scenario: Search for the car product [negative]
    When user calls endpoint car
    Then he sees error message

  Scenario: Search for the cola product with special promo
    When user calls endpoint cola
    Then results shows Coca-cola Zero sugar with promo 4 voor 6.99