package starter.api;

import net.thucydides.core.util.EnvironmentVariables;

abstract class ApiConnector {
    EnvironmentVariables environmentVariables;

    public String getBaseApiUrl(){
        return environmentVariables.getProperty("restapi.baseurl");
    }
}
