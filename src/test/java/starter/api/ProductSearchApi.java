package starter.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.hamcrest.Matchers;
import starter.dataModels.ProductModel;

import java.util.Arrays;
import java.util.List;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class ProductSearchApi extends ApiConnector {
    private static final String PRODUCT_SEARCH_URL = "/v1/search/demo/{product}";

    @Step("Get product info with name {}")
    public void getDataForProductEndpoint(String name) {
        SerenityRest.given()
                .pathParam("product", name)
                .get(getBaseApiUrl() + PRODUCT_SEARCH_URL);
    }

    @Step("Validate empty response")
    public void validateEmptyResponse() {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body("", Matchers.empty()));
    }

    @Step("Validate non empty response")
    public void validateResponse(String title) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body("title", Matchers.hasItem(title)));
    }

    @Step("Validate error response")
    public void validateErrorResponse() {
        restAssuredThat(response -> response.statusCode(404));
        restAssuredThat(response -> response.body("'detail'.'error'", Matchers.equalTo(true)));
    }

    @SneakyThrows
    @Step("Extract product results")
    public List<ProductModel> getProductResults() {
        ObjectMapper mapper = new ObjectMapper();
        String productResponse = SerenityRest.lastResponse().asString();
        return Arrays.asList(mapper.readValue(productResponse, ProductModel[].class));
    }
}
