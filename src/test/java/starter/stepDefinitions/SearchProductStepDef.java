package starter.stepDefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import starter.api.ProductSearchApi;
import starter.dataModels.ProductModel;

import java.util.List;

public class SearchProductStepDef {

    @Steps
    public ProductSearchApi productSearchApi;

    @When("user calls endpoint {}")
    public void callSearchEndpoint(String endpoint) {
        productSearchApi.getDataForProductEndpoint(endpoint);
    }

    @Then("he sees empty response")
    public void checkEmptyResult() {
        productSearchApi.validateEmptyResponse();
    }

    @Then("in the results he sees {}")
    public void checkColaResult(String title) {
        productSearchApi.validateResponse(title);
    }

    @Then("he sees error message")
    public void checkErrorResult() {
        productSearchApi.validateErrorResponse();
    }

    @Then("results shows {} with promo {}")
    public void checkPromoForProduct(String productName, String promo){
        List<ProductModel> products = productSearchApi.getProductResults();
        ProductModel product = products.stream().filter(item -> item.getTitle().equals(productName)).findFirst().get();
        Assert.assertEquals(promo, product.getPromoDetails());
    }
}
