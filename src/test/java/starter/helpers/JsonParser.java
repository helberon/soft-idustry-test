package starter.helpers;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

import static starter.helpers.JsonConfiguration.GLOBAL_OBJECT_MAPPER;

@UtilityClass
public class JsonParser {
    @SneakyThrows
    public <T> String convertObjectToJson(T obj){
        return GLOBAL_OBJECT_MAPPER.writeValueAsString(obj);
    }
}
